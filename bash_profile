export GREP_OPTIONS="--color=auto"
export GREP_COLOR="4;33"
export CLICOLOR="auto"

alias ls="ls -G"
[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm" # This loads RVM into a shell session.
PS1='\n[\u] \[\033[1;33m\]\w\a\[\033[0m\]$\n\$ '
alias mysql=/usr/local/mysql/bin/mysql
alias mysqldump=/usr/local/mysql/bin/mysqldump
alias s='rails s'

source ~/git-completion.bash

alias gco='git co'
alias gci='git ci'
alias grb='git rb'

##
# Your previous /Users/brianstorti/.bash_profile file was backed up as /Users/brianstorti/.bash_profile.macports-saved_2011-07-06_at_21:09:58
##

# MacPorts Installer addition on 2011-07-06_at_21:09:58: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:Library/PostgreSQL/9.0/bin:/Library/PostgreSQL/9.0/bin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.

# aliases for rails
alias db='rails db'
alias backup_socialfrete="backup perform --trigger backup_socialfrete --config-file ~/Dropbox/projects/socialfrete/backup/config.rb"
alias ci="cd ~/Downloads/thoughtworks-cruisecontrol.rb-98fa322/ && ./cruise start"
alias d="echo 'backup decrypt --encryptor openssl --base64 --in [name] --out [name]'"

function cd {
    builtin cd "$@" && ls
}
