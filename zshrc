# Path to your oh-my-zsh configuration.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
export ZSH_THEME="prose"

# Set to this to use case-sensitive completion
# export CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# export DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# export DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# export DISABLE_AUTO_TITLE="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git)

source $ZSH/oh-my-zsh.sh
source ~/.rvm/scripts/rvm

# Customize to your needs...
#export PATH=/opt/local/bin:/opt/local/sbin:/Users/brianstorti/.rvm/bin:/Users/brianstorti/.rvm/gems/ruby-1.9.2-p180@rails31/bin:/Users/brianstorti/.rvm/gems/ruby-1.9.2-p180@global/bin:/Users/brianstorti/.rvm/rubies/ruby-1.9.2-p180/bin:/Users/brianstorti/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/X11/bin
#[[ -s $HOME/.rvm/scripts/rvm ]]


bindkey    "^[[3~"          delete-char
bindkey    "^[3;5~"         delete-char
alias p="cd ~/Dropbox/scripts && ./gera_proposta.rb"
alias prop="cd ~/Dropbox/scripts && ruby gerar_proposta.rb"
alias o="ruby organizer.rb"
alias m="ruby multiple_compare.rb"
alias rf="rm -rf"
alias ccvp="cp ~/Downloads/skype/cvp.txt ."
alias zz="funcoeszz"
function chpwd() {
  emulate -L zsh
  ls -a
}

# Instalacao das Funcoes ZZ (www.funcoeszz.net)
export ZZPATH=/usr/bin/funcoeszz
source /Users/brianstorti/.zzzshrc


weather () {
  w3m -dump "http://www.google.com/search?hl=en&lr=&client=firefox-a&rls=org.mozilla%3Aen-US%3Aofficial&q=weather+${1}&btnG=Search" >& /tmp/weather
  grep -A 5 -m 1 "Weather for" /tmp/weather| cut -c28-
  rm /tmp/weather
}

